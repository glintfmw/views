<?php declare (strict_types=1);
	namespace GlintFMW\Templating\Configuration;

	use GlintFMW\Configuration\Providers\Map;
	use GlintFMW\Configuration\Exception\IntegrityException;
    use GlintFMW\Dependencies\Injector;
    use GlintFMW\Resources\URL;
    use GlintFMW\Templating\RenderService;
    use GlintFMW\Types\Closure;

    /**
	 * Configuration provider for the templating system
	 *
	 * @author Alexis Maiquez Murcia <almamu@almamu.com>
     * @package GlintFMW\Templating\Configuration
	 */
	class TemplatingProvider extends Map
	{
	    /** @var string The default extension for the template files */
	    private const DEFAULT_EXTENSION = '.phtml';

		/** @var bool Indicates if the configuration was already generated */
		private static bool $configurationGenerated = false;

		/** @var array<string, string> Default functions for templating system */
		private static array $defaultFunctions = array (
			"startblock"    => "startblock@GlintFMW\\Templating\\RenderService",
			"endblock"      => "endblock@GlintFMW\\Templating\\RenderService",
			"parent"        => "parent@GlintFMW\\Templating\\RenderService",
			"showblock"     => "showblock@GlintFMW\\Templating\\RenderService",
			"isblock"       => "isblock@GlintFMW\\Templating\\RenderService",
			"extend"        => "render@GlintFMW\\Templating\\RenderService",
			"resource"      => "resource@GlintFMW\\Templating\\RenderService",
			"url"           => "url@GlintFMW\\Templating\\RenderService",
			"current"       => "current@GlintFMW\\Templating\\RenderService"
		);

		/** @var array<string, mixed> The list of default parameters for the templating */
		private static array $defaultParameters = array (
		    'glintfmw_templating_version'   => 1
        );

		function __construct (array $configuration, Injector $dependencyInjector)
        {
            parent::__construct ($configuration, $dependencyInjector);
        }

        /** @inheritdoc */
		static function provides () : string
		{
			return "appTemplating";
		}

		/** @inheritdoc */
		static function convert (array $input)
		{
		    $configuration = array (
		        'dir' => 'templates',
                'functions' => array (),
                'parameters' => self::$defaultParameters,
                'extension' => self::DEFAULT_EXTENSION
            );

		    $defaultFunctions = self::$defaultFunctions;

		    if (array_key_exists ('functions', $input) === true)
                $defaultFunctions = array_merge ($defaultFunctions, $input ['functions']);

		    if (array_key_exists ('parameters', $input) === true)
                $configuration ['parameters'] = array_merge ($configuration ['parameters'], $input ['parameters']);

		    if (array_key_exists ('dir', $input) === true)
            {
                $directory = $input ['dir'];

                if (file_exists ($directory) === false)
                    throw new IntegrityException ("The template directory {$input ['dir']} doesn't exist");

                $configuration ['dir'] = $directory;
            }

		    $configuration ['dir'] = rtrim ($configuration ['dir'], '/');

		    // loop through the functions and convert them to callbacks
            foreach ($defaultFunctions as $name => $controller)
            {
                $configuration ['functions'] [$name] = URL::parseFunctionURL ($controller);
            }

            if (array_key_exists ('domain', $input) === false)
                throw new IntegrityException ("The domain is not present in the templating configuration");
            if (array_key_exists ('path', $input) === false)
                throw new IntegrityException ("The path is not present in the templating configuration");

            $configuration ['domain'] = $input ['domain'];
            $configuration ['path'] = rtrim ($input ['path'], '/');

            if (array_key_exists ('protocol', $input) === true)
            {
                $configuration ['protocol'] = $input ['protocol'];
            }
            else
            {
                $configuration ['protocol'] = '://';
            }

            if (array_key_exists ('extension', $input) === true)
                $configuration ['extension'] = $input ['extension'];

            self::$configurationGenerated = true;

            return $configuration;
		}

		/**
		 * Register a function for the templating provider
		 * ensuring that it's available on runtime as soon
		 * as the templating system is loaded
		 *
		 * @param string $name The name of the function
		 * @param string $lambda The function to call in the format function@class
		 *
		 * @throws IntegrityException If the templating configuration has already been generated or the function exists
		 */
		static function registerDefaultFunction (string $name, string $lambda): void
		{
			if (array_key_exists ($name, self::$defaultFunctions) == true)
				throw new IntegrityException ("The default function {$name} is already registered");
			if (self::$configurationGenerated === true)
				throw new IntegrityException ("The templating system configuration has already been generated. You might want to put templating provider as the last provider to be loaded");

			self::$defaultFunctions [$name] = $lambda;
		}

		/**
		 * Registers a default parameter on runtime
		 *
		 * @param string $parameter The parameter's name
		 * @param mixed $value The parameter's value
		 *
		 * @throws IntegrityException
		 */
		static function registerDefaultParameter (string $parameter, $value): void
		{
		    if (array_key_exists ($parameter, self::$defaultParameters) === true)
		        throw new IntegrityException ("The default parameter {$parameter} is already registered");
		    if (self::$configurationGenerated === true)
                throw new IntegrityException ("The templating system configuration has already been generated. You might want to put your configuration provider as higher priority than " . self::priority ());

		    self::$defaultParameters [$parameter] = $value;
		}

		/**
		 * Returns the template directory used
		 *
		 * @return string The template directory
		 */
		function getTemplatesDir (): string
		{
		    return $this->getConfiguration () ['dir'] ?? '';
		}

		/**
		 * Returns custom template functions
		 *
		 * @return array<Closure> The template functions array
		 */
		function getFunctions (): array
		{
		    return $this->getConfiguration () ['functions'] ?? array ();
		}

		/**
		 * Default variables for the template rendering
		 *
		 * @return array<string, mixed> The default variables for the template rendering
		 */
		function getTemplateDefaultParameters (): array
		{
		    return $this->getConfiguration () ['parameters'] ?? array ();
		}

		/** @return string The configured domain for the app */
		function getDomain (): string
		{
		    return $this->getConfiguration () ['domain'] ?? '';
		}

		/** @return string The public path to the website's base URL */
		function getPublicPath (): string
		{
		    return $this->getConfiguration () ['path'] ?? '';
		}

		/** @return string The default protocol for the routing system */
		function getDefaultProtocol (): string
		{
		    return $this->getConfiguration () ['protocol'] ?? '';
		}

		/** @return string The extension of the template files */
		function getTemplatesExtension (): string
		{
		    return $this->getConfiguration () ['extension'] ?? '.phtml';
		}
	};