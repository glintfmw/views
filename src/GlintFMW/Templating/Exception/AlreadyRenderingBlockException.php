<?php declare (strict_types=1);
	namespace GlintFMW\Templating\Exception;

    class AlreadyRenderingBlockException extends \Exception
    {

    };