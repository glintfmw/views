<?php declare (strict_types=1);
    namespace GlintFMW\Tests\Views;

    use GlintFMW\Dependencies\Injector;
    use GlintFMW\Templating\Configuration\TemplatingProvider;

    use GlintFMW\Templating\RenderService;
    use PHPUnit\Framework\TestCase;

    use org\bovigo\vfs\vfsStreamDirectory;
    use org\bovigo\vfs\vfsStream;

    class RenderingTest extends TestCase
    {
        static Injector $injector;
        static vfsStreamDirectory $fileSystem;
        static vfsStreamDirectory $templatesDirectory;

        static array $routingConfiguration = array (
            "dir" => "",
            "functions" => array (),
            "parameters" => array (
                "developedBy" => "Alexis Maiquez",
                "contactMail" => "almamu@almamu.com",
                "blogTitle" => "The GlintFMW Blog",
                "blogSubtitle" => "Based on bootstrap's blog example template",
                "externalUrls" => array (
                    array ("name" => "GitHub", "url" => "https://github.com/Almamu"),
                    array ("name" => "Twitter", "url" => "https://twitter.com/AlmamuPP"),
                    array ("name" => "BitBucket", "url" => "https://bitbucket.com/Almamu")
                )
            ),
            "domain" => "localhost:8080",
            "path" => "/",
            "protocol" => "http://"
        );

        static function setUpBeforeClass(): void
        {
            self::$injector = new Injector ();
            // setup vfs folders
            self::$fileSystem = vfsStream::setup ();
            self::$templatesDirectory = vfsStream::newDirectory ('templates')->at (self::$fileSystem);
            self::$routingConfiguration ['dir'] = self::$templatesDirectory->url ();
            // create files to be used in the templating tests
            vfsStream::newFile ('main.phtml')
                ->at (self::$templatesDirectory)
                ->setContent ('Hello world!');
            vfsStream::newFile ('parent.phtml')
                ->at (self::$templatesDirectory)
                ->setContent ('<?= $showblock (\'content\') ?> world!');
            vfsStream::newFile ('function.phtml')
                ->at (self::$templatesDirectory)
                ->setContent ('<?php $startblock (\'content\') ?>Hello<?php $endblock () ?><?= $extend (\'parent\');');
        }

        function testConfigurationGeneration ()
        {
            $configuration = TemplatingProvider::convert (self::$routingConfiguration);
            self::$injector->registerDependency (new TemplatingProvider ($configuration, self::$injector));

            $renderService = self::$injector->inject (RenderService::class);

            ob_start ();
            $renderService->render ('main');
            $this->assertEquals ('Hello world!', ob_get_contents ());
            ob_end_clean ();

            ob_start ();
            $renderService->render ('function');
            $this->assertEquals ('Hello world!', ob_get_contents ());
            ob_end_clean ();
        }

    };