<?php declare(strict_types=1);
	/**
	 * @author Almamu
	 */
	namespace GlintFMW\Templating;

	use GlintFMW\Dependencies\Injector;
    use GlintFMW\Templating\Exception\AlreadyRenderingBlockException;
    use GlintFMW\Templating\Exception\BlockNotRegisteredException;
    use GlintFMW\Templating\Exception\NotRenderingBlockException;
    use GlintFMW\Templating\Exception\MissingTemplateException;
	use GlintFMW\Templating\Exception\EmptyBlockNameException;

	use GlintFMW\Templating\Configuration\TemplatingProvider;

	/**
	 * Render functions for templating
     *
     * @author Alexis Maiquez Murcia <almamu@almamu.com>
     * @package GlintFMW\Templating
	 */
	class RenderService
	{
	    /** @var string The current block being rendered (if any) */
		private string $currentblock = "";
        /** @var array<string, string> List of rendered blocks and their contents */
		private array $blocks = array();
		/** @var TemplatingProvider The configuration provider for the templating system */
		private TemplatingProvider $templatingProvider;
		/** @var Injector The dependency injector to use */
		private Injector $dependencyInjector;

		function __construct (TemplatingProvider $templatingProvider, Injector $dependencyInjector)
        {
            $this->templatingProvider = $templatingProvider;
            $this->dependencyInjector = $dependencyInjector;
        }

		/**
		 * Begins a block rendering
		 *
		 * @param string $name The name of the block
         *
         * @throws EmptyBlockNameException If the block name is empty
         * @throws AlreadyRenderingBlockException If we are already rendering a block
		 */
		function startblock (string $name): void
		{
			if (empty ($name) == true)
			{
				throw new EmptyBlockNameException ("The block name cannot be empty");
			}

			if(empty ($this->currentblock) == false)
			{
				throw new AlreadyRenderingBlockException ("Cannot start a block in a block. Block " . $this->currentblock . " is being rendered yet");
			}

			$this->currentblock = $name;

			// begin output buffering
			ob_start ();
		}

        /**
         * Completes the block rendering already being processed
         *
         * @throws NotRenderingBlockException
         */
		function endblock (): void
		{
			if (empty ($this->currentblock) == true)
			{
				throw new NotRenderingBlockException ("Cannot end a block because we are not rendering anything");
			}

			$ob = ob_get_clean ();

			if (is_string ($ob) === false)
			    throw new \Exception ("Cannot perform ob_get_clean");

			$this->blocks [$this->currentblock] = $ob;
			$this->currentblock = "";
		}

        /**
         * Outputs the given block to the standard output
         *
         * @param string $block
         * @throws BlockNotRegisteredException If the specified block is nowhere to be found
         */
		function showblock (string $block): void
		{
			if (array_key_exists ($block, $this->blocks) == false)
			{
				throw new BlockNotRegisteredException("The given block {$block} cannot be displayed as it's not registered");
			}

			echo $this->blocks [$block];
		}

        /**
         * Outputs the current block's registered content (empty if the block is not registered already)
         * @throws BlockNotRegisteredException
         */
		function parent (): void
		{
			if (array_key_exists ($this->currentblock, $this->blocks) == true)
			{
				$this->showblock ($this->currentblock);
			}
		}

		/**
		 * @return bool If we're rendering a block or not
		 */
		function isblock (): bool
		{
			return empty ($this->currentblock) == false;
		}

        /**
         * Renders the given $view with the specified params
         *
         * @param string $view The view file to render
         * @param array<string, mixed> $params Information for the view file to render correctly
         *
         * @throws MissingTemplateException If the template file specified cannot be found
         * @throws \GlintFMW\Dependencies\Exceptions\CacheNotBuiltException
         * @throws \GlintFMW\Dependencies\Exceptions\DependencyNotRegisteredException
         * @throws \ReflectionException
         * @throws \GlintFMW\Types\Exceptions\ClassTypeDoesNotMatchException
         */
		function render (string $view, array $params = array ()): void
		{
			$view = ltrim ($view, "/");

			$defaultparams = $this->templatingProvider->getTemplateDefaultParameters ();
			$functions = $this->templatingProvider->getFunctions ();
			$templatefile = "{$this->templatingProvider->getTemplatesDir ()}/{$view}{$this->templatingProvider->getTemplatesExtension ()}";
			
			if (file_exists ($templatefile) == false)
			{
				throw new MissingTemplateException ("The template file {$templatefile} cannot be found");
			}

			// resolve dependencies on the function list
            $functionsResolved = array ();

			foreach ($functions as $name => $function)
            {
                /** @var \GlintFMW\Types\Closure $function */
                if ($function->isFunction () === true)
                    $functionsResolved [$name] = $function->getClosure ();
                else
                    $functionsResolved [$name] = $function->getClosure (
                        $this->dependencyInjector->resolve ($function->getClass ())
                    );
            }

			extract ($params);
			extract ($defaultparams);
			extract ($functionsResolved);

			include $templatefile;
		}

		/**
		 * Generates a public path to the given resource
		 *
		 * @param string $name The name of the resource
		 *
		 * @return string The public path to the specified resource
		 */
		function resource (string $name): string
		{
			return
                $this->templatingProvider->getDefaultProtocol () .
                $this->templatingProvider->getDomain () .
                $this->templatingProvider->getPublicPath () .
                "/assets/{$name}";
		}

		/**
		 * Generates a full URL to the given $path
		 *
		 * The parameter matching for $urlparams is done searching (and replacing)
		 * the key name in the URL this way:
		 * /hello/:name -> /hello/almamu
		 *
		 * given the following $urlparams: array('name' => 'almamu')
		 *
		 * @param string $path The path to generate the URL for
		 * @param array<string, mixed> $params The GET params for the URL
		 * @param array<string, string> $urlparams Parameters for the URL
		 *
		 * @return string The path to generate
		 */
		function url (string $path, array $params = array (), array $urlparams = array ()): string
		{
			$getparams = ((count ($params) == 0 ) ? '' : '?');

			foreach($params as $key => $value)
			{
				$getparams .= $key . "=" . urlencode ((string) $value) . "&";
			}

			$endurlparams = array();
			$endurlvalues = array();

			foreach($urlparams as $key => $value)
			{
				$endurlparams [] = ":" . $key;
				$endurlvalues [] = $value;
			}

			$path = trim (str_replace ($endurlparams, $endurlvalues, $path), "/");

			return
                $this->templatingProvider->getDefaultProtocol () .
                $this->templatingProvider->getDomain () .
                $this->templatingProvider->getPublicPath () .
                "/{$path}{$getparams}";
		}

		/**
		 * @return string The current, full URL the user is currently at
		 */
		function current (): string
		{
		    /// TODO: USE REQUEST
			return $_SERVER ['REQUEST_URI'];
		}
	};